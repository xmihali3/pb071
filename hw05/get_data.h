#ifndef GET_DATA_H
#define GET_DATA_H

#include <stdbool.h>
#include <stdio.h>

struct path
{
    char name[256];
    double size;
    size_t measure;
    struct path *paths;
    size_t paths_count;
    bool error;
};
bool load_path(struct path *path, const char *cur_path, bool a_switch, char *name);
bool load_dir(struct path *path, const char *cur_path, bool a_switch, char *name);
double get_right_size(struct path *path);
int sort_by_name(const void *a, const void *b);
int sort_by_size(const void *a, const void *b);
void recursive_sort_name(struct path *path);
void recursive_sort_size(struct path *path);

#endif