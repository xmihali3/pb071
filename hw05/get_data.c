#include <ctype.h>
#include <dirent.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

struct path
{
    char name[256];
    double size;
    size_t measure;
    struct path *paths;
    size_t paths_count;
    bool error;
};

bool load_path(struct path *path, const char *cur_path, bool a_switch, char *name)
{
    struct stat statbuf;
    if (stat(cur_path, &statbuf)) {
        fprintf(stderr, "stat error of file: %s\n", cur_path);
        path->error = true;
        return false;
    }

    path->size = a_switch ? statbuf.st_size : (statbuf.st_blocks * 512);
    path->measure = 0;
    path->paths_count = 0;
    path->paths = NULL;
    strcpy(path->name, name);
    path->error = false;
    return true;
}

bool load_dir(struct path *path, const char *cur_path, bool a_switch, char *name)
{
    DIR *dir;
    struct dirent *entry;
    char new_path[4096];

    if (!load_path(path, cur_path, a_switch, name)) {
        return true;
    }

    if ((dir = opendir(cur_path)) == NULL) {
        fprintf(stderr, "Opendir error of file: %s\n", cur_path);
        path->error = true;
        return true;
    }

    while ((entry = readdir(dir)) != NULL) {
        struct stat statbuf;
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        snprintf(new_path, 4096, "%s/%s", cur_path, entry->d_name);

        if (stat(new_path, &statbuf)) {
            fprintf(stderr, "stat error of file: %s\n", new_path);
            continue;
        }

        if (!S_ISREG(statbuf.st_mode) && !S_ISDIR(statbuf.st_mode)) {
            continue;
        }

        path->paths = realloc(path->paths, (++path->paths_count * sizeof(struct path)));
        if (path->paths == NULL) {
            perror("Memory allocation failure");
            return false;
        }

        if (S_ISDIR(statbuf.st_mode)) {
            if (!load_dir(&path->paths[path->paths_count - 1], new_path, a_switch, entry->d_name)) {
                return false;
            }
        }
        else {
            load_path(&path->paths[path->paths_count - 1], new_path, a_switch, entry->d_name);
        }
    }

    for (size_t i = 0; i < path->paths_count; i++) {
        path->size += path->paths[i].size;
        if (path->paths[i].error) {
            path->error = true;
        }
    };

    if (closedir(dir)) {
        fprintf(stderr, "Closedir error of file: %s\n", cur_path);
        path->error = true;
    }
    return true;
}

double get_right_size(struct path *path)
{
    const size_t bytes_ratio = 1024;
    while (path->size >= bytes_ratio) {
        path->measure++;
        path->size /= bytes_ratio;
    }
    return floor(path->size * 10) / 10;
}

int sort_by_name(const void *a, const void *b)
{
    struct path path_a = *(struct path *) a;
    struct path path_b = *(struct path *) b;
    int i = -1;
    char *name_a = path_a.name;
    char *name_b = path_b.name;
    while (name_a[++i]) {
        if (!name_b[i]) {
            return 1;
        }
        if (tolower(name_a[i]) != tolower(name_b[i])) {
            return tolower(name_a[i]) - tolower(name_b[i]);
        }
        if (name_a[i] != name_b[i]) {
            return name_a[i] - name_b[i];
        }
    }
    return -1;
}

int sort_by_size(const void *a, const void *b)
{
    struct path path_a = *(struct path *) a;
    struct path path_b = *(struct path *) b;
    return path_b.size - path_a.size;
}

void recursive_sort_name(struct path *path)
{
    for (size_t i = 0; i < path->paths_count; i++) {
        recursive_sort_name(&path->paths[i]);
    }
    qsort(path->paths, path->paths_count, sizeof(struct path), sort_by_name);
}

void recursive_sort_size(struct path *path)
{
    for (size_t i = 0; i < path->paths_count; i++) {
        recursive_sort_size(&path->paths[i]);
    }
    qsort(path->paths, path->paths_count, sizeof(struct path), sort_by_size);
}
