#include "get_data.h"

#include <ctype.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

struct options
{
    bool a_switch;
    bool size_sort;
    bool percent;
    ssize_t depth;
};

const char *measurements[] = { "B  ", "KiB", "MiB", "GiB", "TiB", "PiB" };

bool check_arguments(int argc, char *argv[], struct options *options)
{
    options->a_switch = false;
    options->size_sort = false;
    options->percent = false;
    options->depth = -1;
    if (argc < 2) {
        return false;
    }
    for (int i = 1; i < argc - 1; i++) {
        if (strcmp(argv[i], "-a") == 0) {
            if (options->a_switch) {
                return false;
            }
            options->a_switch = true;
        } else if (strcmp(argv[i], "-s") == 0) {
            if (options->size_sort) {
                return false;
            }
            options->size_sort = true;
        } else if (strcmp(argv[i], "-p") == 0) {
            if (options->percent) {
                return false;
            }
            options->percent = true;
        } else if (strcmp(argv[i], "-d") == 0) {
            double depth = strtof(argv[++i], NULL);
            if (options->depth != -1
                || depth < 0
                || (depth == 0 && (argv[i][0] != '0' || strlen(argv[i]) != 1))
                || depth != floor(depth)) {
                return false;
            }
            options->depth = depth;
        } else {
            return false;
        }
    }

    return true;
}

void print_data(struct path *path, bool error, struct options *options, size_t full_size, char *indentation, ssize_t cur_depth)
{
    size_t length = strlen(indentation);
    char new_indentation[length + 4];
    char chopped_indentation[length + 1];
    
    if (error) {
        if (path->error) {
            printf("? ");
        }
        else {
            printf("  ");
        }
    }

    if (options->percent) {
        double size = floor(path->size / full_size * 1000) / 10;
        printf("%5.1f%% %s%s\n", size, indentation, path->name);
    } else {
        double size = get_right_size(path);
        printf("%6.1f %s %s%s\n", size, measurements[path->measure], indentation, path->name);
    }

    if (options->depth == cur_depth) {
        return;
    }

    if (length != 0) {
        bool last = (indentation[length - 4] == '\\');
        indentation[length - 4] = 0;
        if (last) {
            snprintf(chopped_indentation, length + 1, "%s    ", indentation);
        }
        else {
            snprintf(chopped_indentation, length + 1, "%s|    ", indentation);
        }
    } else {
        chopped_indentation[0] = 0;
    }

    for (size_t i = 0; i < path->paths_count; i++) {
        if (i + 1 == path->paths_count) {
            snprintf(new_indentation, length + 5, "%s\\-- ", chopped_indentation);
            print_data(&path->paths[i], error, options, full_size, new_indentation, cur_depth + 1);
        } else {
            snprintf(new_indentation, length + 5, "%s|-- ", chopped_indentation);
            print_data(&path->paths[i], error, options, full_size, new_indentation, cur_depth + 1);
        }
    }
}

void free_struct(struct path *path)
{
    for (size_t i = 0; i < path->paths_count; i++) {
        free_struct(&path->paths[i]);
    }
    free(path->paths);
}

int main(int argc, char *argv[])
{
    struct options options;
    if (!check_arguments(argc, argv, &options)) {
        perror("Bad arguments");
        return EXIT_FAILURE;
    }

    struct path main_path;
    struct stat statbuf;
    if (stat(argv[argc - 1], &statbuf)) {
        fprintf(stderr, "stat error of file: %s\n", argv[argc - 1]);
        return EXIT_FAILURE;
    }
    main_path.paths = NULL;
    main_path.paths_count = 0;
    if (S_ISDIR(statbuf.st_mode)) {
        if (!load_dir(&main_path, argv[argc - 1], options.a_switch, argv[argc - 1])) {
            free_struct(&main_path);
            return EXIT_FAILURE;
        }
    }
    else if (S_ISREG(statbuf.st_mode)) {
        load_path(&main_path, argv[argc - 1], options.a_switch, argv[argc - 1]);
    }
    else {
        return EXIT_SUCCESS;
    }

    recursive_sort_name(&main_path);
    if (options.size_sort) {
        recursive_sort_size(&main_path);
    }
    print_data(&main_path, main_path.error, &options, main_path.size, "", 0);

    free_struct(&main_path);
    return EXIT_SUCCESS;
}