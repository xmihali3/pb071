#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

uint32_t spare_cards(int count, uint8_t cards[count]);
uint8_t straight(int count, uint8_t cards[count]);

/* Parse the number of players at the table for the bonus extension.
 * IMPORTANT: Do not modify this function! */
static int parse_players(int argc, char **argv)
{
    switch (argc) {
    case 1:
        return 2;
    case 2:
        return atoi(argv[1]);
    default:
        return 0;
    }
}

uint8_t form_card(char card_number, char card_color)
{
    // all card values
    const char allnums[13] = { '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A' };
    // all card suits (hearts, diamonds, spades, clubs)
    const char allcolors[4] = { 'h', 'd', 's', 'c' };
    uint8_t number = 0;
    uint8_t color = 0;
    for (uint8_t i = 0; i < 13; i++) {
        if (allnums[i] == card_number) {
            number = (i + 1) << 2;
            break;
        }
    }

    for (uint8_t i = 0; i < 4; i++) {
        if (allcolors[i] == card_color) {
            color = i;
            break;
        }
    }

    if (number == 0 || (color == 0 && card_color != 'h')) {
        return 0;
    }

    return number | color;
}

uint8_t check_flush(uint8_t cards[7])
{
    int colors[4] = { 0 };
    char card_color = 0x03;
    for (int card = 0; card < 7; card++) {
        int color = cards[card] & card_color;
        colors[color]++;
    }
    for (int i = 0; i < 4; i++) {
        if (colors[i] >= 5) {
            return i;
        }
    }
    return 4;
}

uint8_t check_straight(int count, uint8_t cards[count])
{
    uint8_t highest_straight_card = 0;
    int in_row = 0;
    for (int num = 0; num < 14; num++) {
        bool straight = false;
        for (int i = 0; i < count; i++) {
            if ((cards[i] >> 2) == num || ((num == 0) && ((cards[i] >> 2) == 13))) {
                in_row++;
                straight = true;
                break;
            }
        }
        if (!straight) {
            in_row = 0;
        } else if (in_row >= 5) {
            highest_straight_card = num;
        }
    }
    return highest_straight_card;
}

uint32_t check_best_cards(int count, uint8_t cards[count])
{
    uint32_t best_cards = 0;
    uint8_t all_nums[13] = { 0 };
    for (int i = 0; i < count; i++) {
        all_nums[(cards[i] >> 2) - 1]++;
    }
    int position = 0;
    int num = 12;
    while (position < 5) {
        if (all_nums[num]-- != 0) {
            best_cards += (num + 1) << (4 * position++);
        } else {
            num--;
        }
    }
    return best_cards;
}

uint32_t flush_or_straight_flush(int* cur_high_card, int* cur_best_comb, uint8_t checking_hand[7])
{
    uint8_t flush_color = check_flush(checking_hand);
    if (flush_color != 4) {
        uint8_t flush_hand[7] = { 0 };
        int flush_count = 0;
        char card_color = 0x03;
        for (int i = 0; i < 7; i++) {
            if ((checking_hand[i] & card_color) == flush_color) {
                flush_hand[flush_count++] = checking_hand[i];
            }
        }

        *cur_high_card = check_straight(flush_count, flush_hand);
        if (*cur_high_card != 0) {
            *cur_best_comb = 8;
            return 0;
        } else {
            uint32_t cur_best_cards = check_best_cards(flush_count, flush_hand);
            *cur_high_card = cur_best_cards & 0xf;
            *cur_best_comb = 5;
            return cur_best_cards;
        }
    }
    return 0;
}

uint32_t add_to_best_cards(uint32_t best_cards, int count, uint8_t value)
{
    for (int j = 0; j < count; j++) {
        best_cards += value;
        best_cards <<= 4;
    }
    return best_cards;
}

uint32_t four_of_k(int value, uint8_t all_nums[13])
{
    uint32_t best_cards = 0;
    for (int j = 12; j >= 0; j--) {
        if (all_nums[j] != 0 && j != value - 1) {
            best_cards += j + 1;
            best_cards <<= 4;
            break;
        }
    }
    return add_to_best_cards(best_cards, 4, value) + 7;
}

uint32_t three_of_k_or_full_house(int three_of_k, int pair, uint8_t all_nums[13])
{
    uint32_t best_cards = 0;
    if (pair != 0) {
        best_cards = add_to_best_cards(best_cards, 2, pair);
        return add_to_best_cards(best_cards, 3, three_of_k) + 6;
    }
    int extra = 1;
    for (int j = 12; j >= 0; j--) {
        if (all_nums[j]-- != 0 && (j + 1) != three_of_k) {
            best_cards += (j++ + 1) << (4 * extra);
            if (extra++ == 2) {
                break;
            }
        }
    }
    return add_to_best_cards(best_cards, 3, three_of_k) + 3;
}

uint32_t pair_or_pairs(int pair1, int pair2, uint8_t all_nums[13])
{
    uint32_t best_cards = 0;
    int extra = 1;
    for (int j = 12; j >= 0; j--) {
        if (all_nums[j]-- != 0 && (j + 1) != pair1 && (j + 1) != pair2) {
            best_cards += (j++ + 1) << (4 * extra);
            if (extra++ == ((pair2 != 0) ? 1 : 3)) {
                break;
            }
        }
    }

    if (pair2 != 0) {
        best_cards = add_to_best_cards(best_cards, 2, pair2);
    }
    return add_to_best_cards(best_cards, 2, pair1) + ((pair2 == 0) ? 1 : 2);
}

uint32_t check_same_numbers(uint8_t cards[7])
{
    uint8_t all_nums[13] = { 0 };
    int three_of_k = 0;
    int pair1 = 0;
    int pair2 = 0;
    uint32_t high_card = 0;
    for (int i = 0; i < 7; i++) {
        all_nums[(cards[i] >> 2) - 1]++;
    }

    for (int i = 0; i < 13; i++) {
        if (all_nums[i] == 4) {
            return four_of_k(i + 1, all_nums);
        }

        if (all_nums[i] == 3) {
            if (three_of_k != 0) {
                uint32_t best_cards = add_to_best_cards(0, 2, three_of_k);
                return add_to_best_cards(best_cards, 3, i + 1) + 6; // Full house
            }
            three_of_k = i + 1;
        } else if (all_nums[i] == 2) {
            pair2 = pair1;
            pair1 = i + 1;
        } else if (all_nums[i] == 1) {
            high_card += i + 1;
            high_card <<= 4;
        }
    }

    if (three_of_k != 0) {
        return three_of_k_or_full_house(three_of_k, pair1, all_nums);
    }

    if (pair1 != 0) {
        return pair_or_pairs(pair1, pair2, all_nums);
    }
    return high_card;
}

void compare_hands(int cb_comb, int* b_comb, int ch_card, int* h_card, uint32_t cb_cards, uint32_t* b_cards, bool* draw, int* winner, int player)
{
    if (cb_comb > *b_comb || (cb_comb == *b_comb && ch_card > *h_card)) {
        *b_comb = cb_comb;
        *h_card = ch_card;
        *b_cards = cb_cards;
        *winner = player + 1;
        *draw = false;
    } else if (cb_comb == *b_comb && ch_card == *h_card) {
        *draw = true;
        if (*b_comb == 4 || *b_comb == 8) {
            return;
        }
        int card = 0;
        for (int i = 0; i < 5; i++) {
            uint8_t cur_card = (cb_cards >> (4 * card)) & 0xf;
            uint8_t best_card = (*b_cards >> (4 * card++)) & 0xf;
            if (cur_card > best_card) {
                *b_comb = cb_comb;
                *h_card = ch_card;
                *b_cards = cb_cards;
                *winner = player + 1;
                *draw = false;
                break;
            } else if (cur_card < best_card) {
                *draw = false;
                break;
            }
        }
    }
}

bool check_winning_hand(int players, uint8_t hands[15], uint8_t table[5])
{
    uint32_t best_cards = 0;
    int high_card = 0;
    int best_comb = 0;
    int winner = 0;
    bool draw = false;
    for (int player = 0; player < players; player++) {
        int cur_high_card = 0;
        int cur_best_comb = 0;
        uint8_t checking_hand[7] = { hands[2 * player], hands[2 * player + 1], table[0], table[1], table[2], table[3], table[4] };

        uint32_t cur_best_cards = flush_or_straight_flush(&cur_high_card, &cur_best_comb, checking_hand);

        if (cur_best_comb <= 7 && best_comb <= 7) {
            uint32_t checked_cards = check_same_numbers(checking_hand);
            int cur_comb = checked_cards & 0xf;
            checked_cards >>= 4;
            if (cur_comb >= cur_best_comb) {
                cur_best_comb = cur_comb;
                cur_best_cards = checked_cards;
                cur_high_card = cur_best_cards & 0xf;
            }
        }

        if (cur_best_comb <= 4 && best_comb <= 4) {
            uint8_t high_straight_card = check_straight(7, checking_hand);
            if (high_straight_card != 0) {
                cur_best_comb = 4;
                cur_high_card = high_straight_card;
            }
        }

        compare_hands(cur_best_comb, &best_comb, cur_high_card, &high_card, cur_best_cards, &best_cards, &draw, &winner, player);
    }

    if (draw) {
        printf("Draw\n");
    } else {
        printf("Player %d\n", winner);
    }
    return true;
}

bool input_valid(char number, int deal_cards, uint8_t hands[16], uint8_t table[5], int player, int i)
{
    while (isspace(number)) {
        number = getchar();
    }

    char color = getchar();
    if (feof(stdin)) {
        fprintf(stderr, "not enough cards\n");
        return false;
    }

    if (color == '\n') {
        fprintf(stderr, "%c: invalid card\n", number);
        return false;
    }

    uint8_t card = form_card(number, color);
    if (card == 0) {
        fprintf(stderr, "%c%c: invalid card\n", number, color);
        return false;
    }

    int hand_cards = (deal_cards == 2) ? (player * 2 + i) : (player * 2);
    for (int j = 0; j < hand_cards; j++) {
        if (card == hands[j]) {
            fprintf(stderr, "%c%c: duplicate card\n", number, color);
            return false;
        }
    }

    if (deal_cards == 5) {
        for (int j = 0; j < i; j++) {
            if (card == table[j]) {
                fprintf(stderr, "%c%c: duplicate card\n", number, color);
                return false;
            }
        }
        table[i] = card;
    } else {
        hands[player * 2 + i] = card;
    }

    if ((i < deal_cards - 1) && !isspace(getchar())) {
        fprintf(stderr, "cards not separated with white space\n");
        return false;
    }
    return true;
}

bool one_round(int players)
{
    uint8_t hands[16];
    uint8_t table[5];
    for (int player = 0; player < players + 1; player++) {
        int deal_cards = (player < players) ? 2 : 5;
        for (int i = 0; i < deal_cards; i++) {
            char number = getchar();
            if (player == 0 && i == 0 && feof(stdin)) {
                return true;
            }

            if (!input_valid(number, deal_cards, hands, table, player, i)) {
                return false;
            }
        }

        if (getchar() != '\n') {
            fprintf(stderr, "too many cards in one hand/table\n");
            return false;
        }
    }

    return check_winning_hand(players, hands, table);
}

int main(int argc, char **argv)
{
    int players = parse_players(argc, argv);

    if (players == 0) {
        fprintf(stderr, "invalid program arguments\n");
        return 1;
    }

    while (!feof(stdin)) {
        if (!one_round(players)) {
            return 1;
        }
    }
    return 0;
}
