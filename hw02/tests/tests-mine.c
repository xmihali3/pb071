#define CUT

#include "libs/cut.h"
#include "libs/mainwrap.h"

#include <stdlib.h>

/* Here, you can write your own tests
 *
 * Suggestions:
 *    - more complex combinations, such as triples
 *    - more incorrect inputs
 *
 * For bonus tests with arguments, use ‹app_main_args(ARGUMENTS…)›
 * instead of ‹app_main()›. */



/* A test template: */

/*
TEST(template) {
    INPUT_STRING(
        "PLAYER 1 CARDS\n"
        "PLAYER 2 CARDS\n"
        "BOARD CARDS\n"
    );

    // TODO: Choose the proper main:
    // 1. For basic functionality, use this function:
    app_main();
    // 2: For bonus functionality with program arguments, use this instead:
    app_main_args("ARGUMENT1", "ARGUMENT2", ...);

    ASSERT_FILE(stdout, "EXPECTED OUTPUT\n");
}
*/

TEST(Duplicate_card_table) {
    INPUT_STRING(
        "4s 8d\n"
        "5c 8h\n"
        "7c 7h Ac 8d 2d\n"
    );
    int retval = app_main();
    ASSERT(retval != 0);
}

TEST(Duplicate_card_hand) {
    INPUT_STRING(
        "8d 8d\n"
        "5c 8h\n"
        "7c 7h Ac Kd 2d\n"
    );
    int retval = app_main();
    ASSERT(retval != 0);
}

TEST(invalid_card) {
    INPUT_STRING(
        "8d 8p\n"
        "5c 8h\n"
        "7c 7h Ac Kd 2d\n"
    );
    int retval = app_main();
    ASSERT(retval != 0);
}

TEST(invalid_program_arguments) {
    INPUT_STRING(
        "4s 8d\n"
        "5c 8h\n"
        "7c 7h Ac Kd 2d\n"
        "8d 8p\n"
        "5c 8h\n"
    );
    int retval = app_main();
    ASSERT(retval != 0);
}

TEST(not_enough_cards) {
    INPUT_STRING(
        "4s 8d\n"
        "5c 8h\n"
        "7c 7h Ac Kd 2d\n"
        "8d 8p\n"
        "5c 8h\n"
        "7c 7h Ac 2d\n"
    );
    int retval = app_main();
    ASSERT(retval != 0);
}

TEST(too_many_cards) {
    INPUT_STRING(
        "4s 8d\n"
        "5c 8h\n"
        "7c 7h Ac Kd 2d\n"
        "8d 8p\n"
        "5c 8h\n"
        "7c 7h Ac 2d 4d 5s\n"
    );
    int retval = app_main();
    ASSERT(retval != 0);
}

TEST(P1_win_full_house) {
    INPUT_STRING(
        "6s Kh\n"
        "7d 3h\n"
        "Kc Ks 7h 7s Jd\n"
    );

    app_main();
    ASSERT_FILE(stdout, "Player 1\n");
}

TEST(P1_win_four_of_kind) {
    INPUT_STRING(
        "Kd Kh\n"
        "7d 3h\n"
        "Kc Ks 7h 7s Jd\n"
    );

    app_main();
    ASSERT_FILE(stdout, "Player 1\n");
}

TEST(P1_win_straight) {
    INPUT_STRING(
        "6h 8d\n"
        "7h 3h\n"
        "Ac 4h 5h 7s Jd\n"
    );

    app_main();
    ASSERT_FILE(stdout, "Player 1\n");
}

TEST(P1_win_high_card) {
    INPUT_STRING(
        "9d Td\n"
        "9h 8h\n"
        "7c 2s Ac 3h Kd\n"
    );

    app_main();
    ASSERT_FILE(stdout, "Player 1\n");
}

TEST(P2_win_straight_flush) {
    INPUT_STRING(
        "6s Kh\n"
        "7h 3h\n"
        "Ac 4h 5h 6h Jd\n"
    );

    app_main();
    ASSERT_FILE(stdout, "Player 2\n");
}

TEST(P2_win_flush) {
    INPUT_STRING(
        "6s Kh\n"
        "7h 3h\n"
        "Ac 4h 2h 6h Jd\n"
    );

    app_main();
    ASSERT_FILE(stdout, "Player 2\n");
}

TEST(P2_win_three_of_kind) {
    INPUT_STRING(
        "6s Kh\n"
        "7h 3h\n"
        "Ac 3d 3s 6h Jd\n"
    );

    app_main();
    ASSERT_FILE(stdout, "Player 2\n");
}

TEST(P2_win_one_pair) {
    INPUT_STRING(
        "9d Td\n"
        "9h Jh\n"
        "9c 2s 3c 3h 7d\n"
    );

    app_main();
    ASSERT_FILE(stdout, "Player 2\n");
}

TEST(P2_win_two_pairs) {
    INPUT_STRING(
        "9d Td\n"
        "9h 8h\n"
        "9c 2s 2c 3h 8d\n"
    );

    app_main();
    ASSERT_FILE(stdout, "Player 2\n");
}