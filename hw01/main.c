#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool calc(uint8_t bytes[5], int count)
{
    bool checking = true;
    int ones = 0;
    uint8_t check_bit = 1 << 7;
    //loop through all 40 bites
    for (int bit_pos = 0; bit_pos < 40; bit_pos++) {
        if ((bit_pos % count) == 0) {
            checking = !checking;
        }

        if (checking) {
            ones += (check_bit & bytes[bit_pos / 8]) ? 1 : 0;
        }

        check_bit >>= 1;
        if (check_bit < 1) {
            check_bit = 1 << 7;
        }
    }
    return (ones % 2) == 1;
}

bool encode(void)
{
    while (!feof(stdin)) {
        uint8_t result[5] = { 0 };
        uint8_t bit = 1 << 7;
        int bit_pos = 0;
        int exp = 0;
        for (int i = 0; i < 4; i++) {
            char byte = getchar();
            if (feof(stdin)) {
                if (bit_pos == 0) {
                    return true;
                }
                byte = 0x00;
            }
            for (int i = 0; i < 8; i++) {
                if ((byte & 128) && (bit_pos != exp)) {
                    result[bit_pos / 8] |= bit;
                }

                bit >>= 1;
                if (bit < 1) {
                    bit = 1 << 7;
                }

                if ((bit_pos == exp) || (bit_pos == 0)) {
                    i--;
                    exp = (bit_pos == 0) ? 1 : exp * 2;
                    bit_pos++;
                    continue;
                }
                bit_pos++;
                byte <<= 1;
            }
        }

        bit_pos = 1 << 6;
        exp = 1;
        //checks parity bits
        for (int i = 0; i < 6; i++) {
            if (calc(result, exp)) {
                result[exp / 8] |= bit_pos;
            }
            bit_pos = (i < 2) ? bit_pos >> exp : 1 << 7;
            exp *= 2;
        }

        for (int i = 0; i < 5; i++) {
            putchar(result[i]);
        }
    }
    return true;
}

bool decode(void)
{
    int cycle = 0;
    while (!feof(stdin)) {
        char result[4] = { 0 };
        char bytes[5];
        int bit = 1 << 7;
        char error = 0x00;

        for (char pos = 0x00; pos < 40; pos++) {
            if ((pos % 8) == 0) {
                bytes[pos / 8] = getchar();
            }

            if ((pos == 0) && feof(stdin)) {
                return true;
            }

            if (feof(stdin) || ((pos == 0 || pos == 39) && (bytes[pos / 8] & bit))) {
                fprintf(stderr, "Wrong code word\n");
                return false;
            }

            if ((bytes[pos / 8] & bit)) {
                error ^= pos;
            }

            bit >>= 1;
            if (bit < 1) {
                bit = 1 << 7;
            }
        }

        if (error != 0) {
            bit >>= error % 8;
            bytes[error / 8] ^= bit;
            fprintf(stderr, "One-bit error in byte %d\n", error / 8 + 5 * cycle);
        }

        bit = 1 << 7;
        int res_byte = 0;
        int pos = -1;
        float exp = 0.5;
        for (int byte = 0; byte < 5; byte++) {
            for (int i = 0; i < 8; i++) {
                pos++;
                if ((pos == 0) || (pos == 39) || (pos == exp)) {
                    bytes[byte] <<= 1;
                    exp *= 2;
                    continue;
                }

                if (bytes[byte] & 128) {
                    result[res_byte] += bit;
                }

                bytes[byte] <<= 1;
                bit >>= 1;
                if (bit < 1) {
                    bit = 1 << 7;
                    res_byte++;
                }
            }
        }

        //correct byte_count
        for (int i = 0; i < 4; i++) {
            putchar(result[i]);
        }
        cycle++;
    }
    return true;
}

/*************************************
 * DO NOT MODIFY THE FUNCTION BELLOW *
 *************************************/
int main(int argc, char *argv[])
{
    if (argc > 2) {
        fprintf(stderr, "Usage: %s (-e|-d)\n", argv[0]);
        return EXIT_FAILURE;
    }
    bool is_success = false;
    if (argc == 1 || (strcmp(argv[1], "-e") == 0)) {
        is_success = encode();
    } else if (strcmp(argv[1], "-d") == 0) {
        is_success = decode();
    } else {
        fprintf(stderr, "Unknown argument: %s\n", argv[1]);
    }
    return is_success ? EXIT_SUCCESS : EXIT_FAILURE;
}