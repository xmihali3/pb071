/*
 * You can write your own tests in this file
 */

#define CUT
#include "libs/cut.h"

#include "libs/mainwrap.h"

#include <stdlib.h>

// Test template just for you ;)

TEST(my_test_nanecisto_encode_ones)
{
    const unsigned char input[] = {0x01, 0x01, 0x01, 0x01};
    const unsigned char output[] = {0x20, 0x08, 0x04, 0x04, 0x82};

    INPUT_BYTES(input);
    CHECK(app_main("-e") == EXIT_SUCCESS);
    CHECK_FILE(stderr, "");
    CHECK_BINARY_FILE(stdout, output);
}

TEST(my_test_nanecisto_decode_ones)
{
    const unsigned char input[] = {0x20, 0x08, 0x04, 0x04, 0x82};
    const unsigned char output[] = {0x01, 0x01, 0x01, 0x01};

    INPUT_BYTES(input);
    CHECK(app_main("-d") == EXIT_SUCCESS);
    CHECK_FILE(stderr, "");
    CHECK_BINARY_FILE(stdout, output);
}

TEST(my_test_nanecisto_encode_empty)
{
    const unsigned char input[1];
    const unsigned char output[] = {0x00, 0x00, 0x00, 0x00, 0x00};

    INPUT_BYTES(input);
    CHECK(app_main("-e") == EXIT_SUCCESS);
    CHECK_FILE(stderr, "");
    CHECK_BINARY_FILE(stdout, output);
}

TEST(my_test_nanecisto_decode_wrong_code_word)
{
    const unsigned char input[] = {0x20, 0x08, 0x04, 0x04};
    
    INPUT_BYTES(input);
    CHECK(app_main("-d") == EXIT_FAILURE);
    CHECK_FILE(stderr, "Wrong code word\n");
}

TEST(my_test_nanecisto_decode_one_bit_error)
{
    const unsigned char input[] = {0x30, 0x80, 0x04, 0x08, 0x06};
    const unsigned char output[] = {0x00, 0x01, 0x02, 0x03};
    
    INPUT_BYTES(input);
    CHECK(app_main("-d") == EXIT_SUCCESS);
    CHECK_FILE(stderr, "One-bit error in byte 0\n");
    CHECK_BINARY_FILE(stdout, output);
}

TEST(my_test_nanecisto_encode_8_bytes)
{
    const unsigned char input[] = {0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03};
    const unsigned char output[] = {0x20, 0x80, 0x04, 0x08, 0x06, 0x20, 0x80, 0x04, 0x08, 0x06};

    INPUT_BYTES(input);
    CHECK(app_main("-e") == EXIT_SUCCESS);
    CHECK_FILE(stderr, "");
    CHECK_BINARY_FILE(stdout, output);
}

TEST(my_test_nanecisto_decode_10_bytes)
{
    const unsigned char input[] = {0x20, 0x80, 0x04, 0x08, 0x06, 0x20, 0x80, 0x04, 0x08, 0x06};
    const unsigned char output[] = {0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03};

    INPUT_BYTES(input);
    CHECK(app_main("-d") == EXIT_SUCCESS);
    CHECK_FILE(stderr, "");
    CHECK_BINARY_FILE(stdout, output);
}