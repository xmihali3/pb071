#ifndef FILTER_DATA_H
#define FILTER_DATA_H

typedef enum {
    PLASTICS_AND_ALUMINIUM,
    PAPER,
    BIODEGRADABLE_WASTE,
    CLEAR_GLASS,
    COLORED_GLASS,
    TEXTILE,
    NUM_WASTE_TYPES
} WasteType;

extern const char all_type_acronyms[];

WasteType waste_type_from_string(const char *waste_type_string);
char waste_type_acronym(WasteType waste_type);
bool check_waste_types(char waste_type, char *filt_type);
void filter_type(char *filt_type, size_t *filtered_data, size_t *size);
void filter_capacity(char *capacity, size_t *filtered_data, size_t *size);
void filter_public(char *publicity, size_t *filtered_data, size_t *size);
void apply_filters(int argc, char *argv[], size_t *filtered_data, size_t *size);
int filter_data(int argc, char *argv[]);

#endif