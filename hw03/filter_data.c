#include "data_source.h"
#include "print_data.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum {
    PLASTICS_AND_ALUMINIUM,
    PAPER,
    BIODEGRADABLE_WASTE,
    CLEAR_GLASS,
    COLORED_GLASS,
    TEXTILE,
    NUM_WASTE_TYPES
} WasteType;

const char *all_type_strings[] = {
    "Plastics and Aluminium",
    "Paper",
    "Biodegradable waste",
    "Clear glass",
    "Colored glass",
    "Textile"
};

const char all_type_acronyms[] = { 'A', 'P', 'B', 'G', 'C', 'T' };

WasteType waste_type_from_string(const char *waste_type_string)
{
    for (int i = 0; i < NUM_WASTE_TYPES; i++) {
        if (!strcmp(waste_type_string, all_type_strings[i])) {
            return (WasteType)i;
        }
    }
    return NUM_WASTE_TYPES;
}

char waste_type_acronym(WasteType waste_type)
{
    if (waste_type < NUM_WASTE_TYPES) {
        return all_type_acronyms[waste_type];
    } else {
        return 'X';
    }
}

bool check_waste_types(char waste_type, char *filt_type)
{
    for (size_t i = 0; i < strlen(filt_type); i++) {
        if (filt_type[i] == waste_type) {
            return true;
        }
    }
    return false;
}

void filter_type(char *filt_type, struct container *filtered_data, size_t *size)
{
    size_t new_size = 0;
    for (size_t i = 0; i < *size; i++) {
        WasteType waste_type = waste_type_from_string(filtered_data[i].waste_type);
        char waste_type_acr = waste_type_acronym(waste_type);
        if (check_waste_types(waste_type_acr, filt_type)) {
            filtered_data[new_size++] = filtered_data[i];
        }
    }
    *size = new_size;
}

void filter_capacity(char *capacity, struct container *filtered_data, size_t *size)
{
    size_t min_cap = strtoul(strtok(capacity, "-"), NULL, 10);
    size_t max_cap = strtoul(strtok(NULL, "-"), NULL, 10);
    size_t new_size = 0;
    for (size_t i = 0; i < *size; i++) {
        size_t container_cap = filtered_data[i].capacity;
        if (min_cap <= container_cap && container_cap <= max_cap) {
            filtered_data[new_size++] = filtered_data[i];
        }
    }
    *size = new_size;
}

void filter_public(char *publicity, struct container *filtered_data, size_t *size)
{
    size_t new_size = 0;
    for (size_t i = 0; i < *size; i++) {
        if (!strcmp(filtered_data[i].public, publicity)) {
            filtered_data[new_size++] = filtered_data[i];
        }
    }
    *size = new_size;
}

void apply_filters(int argc, char *argv[], struct container *filtered_data, size_t *size)
{
    size_t new_size = *size;
    for (int i = 1; i < argc - 2; i++) {
        if (!strcmp(argv[i], "-t")) {
            filter_type(argv[++i], filtered_data, &new_size);
        } else if (!strcmp(argv[i], "-c")) {
            filter_capacity(argv[++i], filtered_data, &new_size);
        } else {
            filter_public(argv[++i], filtered_data, &new_size);
        }
    }
    *size = new_size;
}

int filter_data(int argc, char *argv[])
{
    struct container *filtered_data = NULL;
    size_t size = -1;
    while (get_container_id((++size)) != NULL) {
        filtered_data = realloc(filtered_data, (size + 1) * sizeof(struct container));
        if (filtered_data == NULL) {
            return EXIT_FAILURE;
        }
        filtered_data[size].id = strtoul(get_container_id(size), NULL, 10);
        filtered_data[size].capacity = strtoul(get_container_capacity(size), NULL, 10);
        filtered_data[size].number = get_container_number(size);
        filtered_data[size].street = get_container_street(size);
        filtered_data[size].waste_type = get_container_waste_type(size);
        filtered_data[size].public = get_container_public(size);
        filtered_data[size].neighbors = NULL;
        filtered_data[size].neighbors_count = 0;
    }

    apply_filters(argc, argv, filtered_data, &size);

    for (size_t i = 0; i < size; i++) {
        print_data_line(&filtered_data[i]);
    }

    free(filtered_data);
    destroy_data_source();
    return EXIT_SUCCESS;
}
