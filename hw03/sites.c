#include "data_source.h"
#include "filter_data.h"
#include "print_data.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct site
{
    size_t number;
    size_t count;
    struct container *containers;
    bool waste_types[6];
    double coor_x;
    double coor_y;
};

void add_new_site(struct site *sites, size_t sites_count, size_t i)
{
    sites[sites_count].number = sites_count + 1;
    sites[sites_count].count = 1;
    sites[sites_count].containers[0].id = strtoul(get_container_id(i), NULL, 10);
    sites[sites_count].containers[0].neighbors = NULL;
    sites[sites_count].containers[0].neighbors_count = 0;
    for (size_t j = 0; j < 6; j++) {
        sites[sites_count].waste_types[j] = false;
    }
    sites[sites_count].waste_types[waste_type_from_string(get_container_waste_type(i))] = true;
    sites[sites_count].coor_x = strtod(get_container_x(i), NULL);
    sites[sites_count].coor_y = strtod(get_container_y(i), NULL);
}

bool check_sites(struct site *sites, size_t sites_count, size_t line_index, bool *alloc_error)
{
    double x = strtod(get_container_x(line_index), NULL);
    double y = strtod(get_container_y(line_index), NULL);
    for (size_t i = 0; i < sites_count; i++) {
        if (x == sites[i].coor_x && y == sites[i].coor_y) {
            sites[i].containers = realloc(sites[i].containers, (sites[i].count + 1) * sizeof(struct container));
            if (sites[i].containers == NULL) {
                *alloc_error = true;
                return true;
            }
            sites[i].waste_types[waste_type_from_string(get_container_waste_type(line_index))] = true;
            sites[i].containers[sites[i].count].id = strtoul(get_container_id(line_index), NULL, 10);
            sites[i].containers[sites[i].count].neighbors = NULL;
            sites[i].containers[sites[i].count++].neighbors_count = 0;
            return true;
        }
    }
    return false;
}

void print_type_of_waste(struct site *site)
{
    for (int i = 0; i < 6; i++) {
        if (site->waste_types[i]) {
            putchar(all_type_acronyms[i]);
        }
    }
    putchar(';');
}

int seek_site(size_t neighbor, struct site *sites, size_t sites_count, size_t **neighbor_sites_ptr, size_t *count)
{
    size_t *neighbor_sites = *neighbor_sites_ptr;
    for (size_t i = 0; i < sites_count; i++) {
        for (size_t j = 0; j < sites[i].count; j++) {
            if (sites[i].containers[j].id == neighbor) {
                for (size_t k = 0; k < *count; k++) {
                    if (i + 1 == neighbor_sites[k]) {
                        return EXIT_SUCCESS;
                    }
                }
                *count += 1;
                neighbor_sites = (size_t *) realloc(neighbor_sites, *count * sizeof(size_t));
                if (neighbor_sites == NULL) {
                    return EXIT_FAILURE;
                }
                neighbor_sites[*count - 1] = i + 1;
                *neighbor_sites_ptr = neighbor_sites;
                return EXIT_SUCCESS;
            }
        }
    }
    return EXIT_SUCCESS;
}

int compare_neighbor_sites(const void *neighbor_a, const void *neighbor_b)
{
    size_t num_a = *(size_t *) neighbor_a;
    size_t num_b = *(size_t *) neighbor_b;
    if (num_a < num_b) {
        return -1;
    }
    return 1;
}

int print_neighbors(struct site *sites, size_t i, size_t sites_count)
{
    for (size_t j = 0; j < sites[i].count; j++) {
        if (check_neighbors(&sites[i].containers[j])) {
            return EXIT_FAILURE;
        }
    }

    size_t *neighbor_sites = NULL;
    size_t n_count = 0;
    bool alloc_error = false;
    for (size_t j = 0; j < sites[i].count; j++) {
        for (size_t k = 0; k < sites[i].containers[j].neighbors_count; k++) {
            if (seek_site(sites[i].containers[j].neighbors[k], sites, sites_count, &neighbor_sites, &n_count)) {
                alloc_error = true;
                break;
            }
        }
    }

    if (alloc_error) {
        free(neighbor_sites);
        return EXIT_FAILURE;
    }
    qsort(neighbor_sites, n_count, sizeof(size_t), compare_neighbor_sites);
    bool next = false;
    for (size_t i = 0; i < n_count; i++) {
        if (neighbor_sites[i] != i) {
            if (next) {
                putchar(',');
            }
            printf("%ld", neighbor_sites[i]);
            next = true;
        }
    }
    free(neighbor_sites);

    printf("\n");
    return EXIT_SUCCESS;
}

int list_of_sites()
{
    struct site *sites = NULL;
    size_t sites_count = 0;
    size_t i = -1;
    bool alloc_error = false;

    while (get_container_id(++i) != NULL) {
        if (!check_sites(sites, sites_count, i, &alloc_error)) {
            sites = realloc(sites, (sites_count + 1) * sizeof(struct site));
            if (sites == NULL) {
                return EXIT_FAILURE;
            }
            sites[sites_count].containers = malloc(sizeof(struct container));
            if (sites[sites_count].containers == NULL) {
                alloc_error = true;
                break;
            }
            add_new_site(sites, sites_count, i);
            sites_count++;
        }

        if (alloc_error) {
            perror("memory allocation fail\n");
            for (size_t i = 0; i < sites_count; i++) {
                free(sites[i].containers);
            }
            free(sites);
            return EXIT_FAILURE;
        }
    }

    for (size_t i = 0; i < sites_count; i++) {
        if (!alloc_error) {
            printf("%ld;", i + 1);
            print_type_of_waste(&sites[i]);
            if (print_neighbors(sites, i, sites_count)) {
                alloc_error = true;
                break;
            }
        }
    }

    for (size_t i = 0; i < sites_count; i++) {
        for (size_t j = 0; j < sites[i].count; j++) {
            free(sites[i].containers[j].neighbors);
        }
        free(sites[i].containers);
    }
    free(sites);
    destroy_data_source();
    return alloc_error;
}
