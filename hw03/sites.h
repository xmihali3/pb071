#ifndef SITES_H
#define SITES_H

struct site;
void add_new_site(struct site *site, size_t sites_count, size_t i);
bool check_sites(struct site **sites, size_t sites_count, size_t line_index, bool *alloc_error);
void print_type_of_waste(struct site *site);
bool neighbor_in_site(size_t neighbor, size_t *site);
int seek_site(size_t neighbor, struct site **sites, size_t sites_count, size_t **neighbor_sites_ptr, size_t *count);
int compare_neighbor_sites(const void *neighbor_a, const void *neighbor_b);
int print_neighbors(struct site **sites, size_t i, size_t sites_count);
void free_struct(struct site *site);
int list_of_sites();

#endif