/*
 * You can use this file for your own tests
 */

#include "libs/cut.h"
#include "libs/mainwrap.h"
#include "libs/utils.h"

#include <stdlib.h>
#include <string.h>

/* The following “extentions” to CUT are available in this test file:
 *
 * • ‹CHECK_IS_EMPTY(file)› — test whether the file is empty.
 * • ‹CHECK_NOT_EMPTY(file)› — inverse of the above.
 *
 * • ‹app_main_args(ARG…)› — call your ‹main()› with given arguments.
 * • ‹app_main()› — call your ‹main()› without any arguments. */

#define CONTAINERS_FILE "tests/data/Brno-BosonohyContainers.csv"
#define PATHS_FILE "tests/data/Brno-BosonohyPaths.csv"

TEST(my_test_read_data)
{
    int rv = 0; /* return value of main()*/
    CHECK(app_main_args(CONTAINERS_FILE, PATHS_FILE) == rv);

    /* TIP: Use ‹app_main()› to test the program without arguments. */

    char correct_output[10416];
    strcat(correct_output, "ID: 23947, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4136 4477 4478 4481 4482 6198 6199 6297 6298 6715 6716 6719 6720 6723 6724 7677 7678 10614 10660 10879 10881 10882 10883 10897 10974 11119 11800 11844 12090 12092 12093 12094 12095 12096 12109 12189 12346 13657 14376 18509 19762 22119 22211 22212 22698 23200 23201 23950 23951 23952 23953 23954 24046 24047 24048 24049 24051 24053 24054 24055 25733 26115 26221 26242\n"
        "ID: 23948, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 23949, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 8300, Type: Clear glass, Capacity: 1550, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 24045, Type: Biodegradable waste, Capacity: 240, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 8301, Type: Colored glass, Capacity: 1550, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 10440, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 11613, Type: Paper, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 17490, Type: Paper, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 22164, Type: Textile, Capacity: 1000, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 11612, Type: Paper, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 23952, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 4136 4477 4478 4481 4482 6198 6199 6715 6716 6719 6720 6723 6724 7677 7678 8300 8301 10440 10614 10660 10879 10881 10883 10897 10974 11119 11612 11613 11800 11844 12090 12092 12093 12109 12189 12346 13657 14376 17490 18509 19762 22119 22164 22211 22212 22698 23200 23947 23948 23949 23950 23953 23954 24045 24046 24047 24048 24049 24051 24053 24054 24055 25733 26115 26221 26242\n"
        "ID: 6298, Type: Clear glass, Capacity: 1125, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 23201, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 12095, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 12094, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 12096, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 10882, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 6297, Type: Colored glass, Capacity: 1125, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 23951, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 22119, Type: Textile, Capacity: 1000, Address: Prazska 702, Neighbors: 4135 4136 4481 4482 6198 6199 6297 6298 6719 6720 6723 6724 8300 8301 10440 10660 10879 10882 10897 10974 11119 11612 11613 11844 12090 12094 12095 12096 12109 12189 12346 13657 14376 17490 18509 22164 22212 22698 23200 23201 23947 23948 23949 23951 23952 23953 23954 24045 24048 24053 24054 24055 26115 26242\n"
        "ID: 7678, Type: Clear glass, Capacity: 2500, Address: Prazska 702, Neighbors: 4135 14376 22698 23947 23952\n");
    strcat(correct_output, "ID: 10883, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 702, Neighbors: 4135 14376 22698 23947 23952\n"
        "ID: 7677, Type: Colored glass, Capacity: 2500, Address: Prazska 702, Neighbors: 4135 14376 22698 23947 23952\n"
        "ID: 4477, Type: Colored glass, Capacity: 1125, Address: Prazska 174, Neighbors: 6198 6199 6715 6716 8300 8301 10440 10614 10660 11612 11613 11800 11844 14376 17490 19762 22164 22211 22698 23947 23948 23949 23952 24045 24046 24047 24048 24049 25733 26115 26221\n"
        "ID: 12093, Type: Paper, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 4478, Type: Clear glass, Capacity: 1125, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 12092, Type: Paper, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 10881, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 23950, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 24051, Type: Biodegradable waste, Capacity: 240, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 4135, Type: Colored glass, Capacity: 1125, Address: Vzhledna 108, Neighbors: 4481 4482 6297 6298 6719 6720 6723 6724 7677 7678 8300 8301 10440 10879 10882 10883 10897 10974 11612 11613 12090 12094 12095 12096 12109 12189 13657 14376 17490 18509 22119 22164 22212 23201 23947 23948 23949 23951 23952 23953 24045 24053 24054 26242\n"
        "ID: 4136, Type: Clear glass, Capacity: 1125, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 12346, Type: Paper, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 23954, Type: Plastics and Aluminium, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 23200, Type: Paper, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 24055, Type: Biodegradable waste, Capacity: 240, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 11119, Type: Plastics and Aluminium, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 6716, Type: Clear glass, Capacity: 1125, Address: K Berce 721, Neighbors: 4477 4478 10881 12092 12093 22698 23947 23950 23952 24049 24051\n"
        "ID: 6715, Type: Colored glass, Capacity: 1125, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 24047, Type: Biodegradable waste, Capacity: 240, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 10614, Type: Plastics and Aluminium, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 22211, Type: Textile, Capacity: 1000, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 19762, Type: Plastics and Aluminium, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 26221, Type: Biodegradable waste, Capacity: 240, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 11800, Type: Paper, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 25733, Type: Paper, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 24046, Type: Biodegradable waste, Capacity: 240, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 22698, Type: Plastics and Aluminium, Capacity: 1100, Address: 466, Neighbors: 4477 4478 4481 4482 6297 6298 6715 6716 7677 7678 8300 8301 10440 10614 10881 10882 10883 10974 11612 11613 11800 12092 12093 12094 12095 12096 12189 14376 17490 19762 22119 22164 22211 23201 23947 23948 23949 23950 23951 23952 23953 24045 24046 24047 24049 24051 24053 24054 25733 26221 26242\n"
        "ID: 6198, Type: Colored glass, Capacity: 1125, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n"
        "ID: 26115, Type: Biodegradable waste, Capacity: 240, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n"
        "ID: 10660, Type: Plastics and Aluminium, Capacity: 1100, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n");
    strcat(correct_output, "ID: 24048, Type: Biodegradable waste, Capacity: 240, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n"
        "ID: 11844, Type: Paper, Capacity: 1100, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n"
        "ID: 6199, Type: Clear glass, Capacity: 1125, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n"
        "ID: 14376, Type: Paper, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 4136 4477 4478 6198 6199 6297 6298 7677 7678 10660 10881 10882 10883 11119 11844 12092 12093 12094 12095 12096 12346 22119 22698 23200 23201 23947 23950 23951 23952 23954 24048 24051 24055 26115\n"
        "ID: 23953, Type: Plastics and Aluminium, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 26242, Type: Biodegradable waste, Capacity: 240, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 10974, Type: Plastics and Aluminium, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 4481, Type: Colored glass, Capacity: 1125, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 24053, Type: Biodegradable waste, Capacity: 240, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 12189, Type: Paper, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 24054, Type: Biodegradable waste, Capacity: 240, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 4482, Type: Clear glass, Capacity: 1125, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 13657, Type: Paper, Capacity: 1100, Address: Prima 331, Neighbors: 4135 4136 6723 6724 10879 11119 12090 12346 22119 22212 23200 23947 23952 23954 24055\n"
        "ID: 10897, Type: Plastics and Aluminium, Capacity: 1100, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 6720, Type: Clear glass, Capacity: 1125, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 12109, Type: Paper, Capacity: 1100, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 6719, Type: Colored glass, Capacity: 1125, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 18509, Type: Plastics and Aluminium, Capacity: 1100, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 6724, Type: Clear glass, Capacity: 1125, Address: Pracata 783, Neighbors: 4135 6719 6720 10897 12109 13657 18509 22119 23947 23952\n"
        "ID: 6723, Type: Colored glass, Capacity: 1125, Address: Pracata 783, Neighbors: 4135 13657 22119 23947 23952\n"
        "ID: 12090, Type: Paper, Capacity: 1100, Address: Pracata 783, Neighbors: 4135 13657 22119 23947 23952\n"
        "ID: 10879, Type: Plastics and Aluminium, Capacity: 1100, Address: Pracata 783, Neighbors: 4135 13657 22119 23947 23952\n"
        "ID: 22212, Type: Textile, Capacity: 1000, Address: Pracata 783, Neighbors: 4135 13657 22119 23947 23952\n"
        "ID: 24049, Type: Biodegradable waste, Capacity: 240, Address: 313, Neighbors: 4477 6715 6716 10614 11800 19762 22211 22698 23947 23952 24046 24047 25733 26221\n");

    ASSERT_FILE(stdout, correct_output);
    CHECK_FILE(stderr, "" /* STDERR is empty*/);
}

TEST(my_test_all_waste_filter_type)
{
    int rv = 0; /* return value of main()*/
    CHECK(app_main_args("-t", "PACGBT", CONTAINERS_FILE, PATHS_FILE) == rv);

    /* TIP: Use ‹app_main()› to test the program without arguments. */

    char correct_output[10416];
    strcat(correct_output, "ID: 23947, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4136 4477 4478 4481 4482 6198 6199 6297 6298 6715 6716 6719 6720 6723 6724 7677 7678 10614 10660 10879 10881 10882 10883 10897 10974 11119 11800 11844 12090 12092 12093 12094 12095 12096 12109 12189 12346 13657 14376 18509 19762 22119 22211 22212 22698 23200 23201 23950 23951 23952 23953 23954 24046 24047 24048 24049 24051 24053 24054 24055 25733 26115 26221 26242\n"
        "ID: 23948, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 23949, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 8300, Type: Clear glass, Capacity: 1550, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 24045, Type: Biodegradable waste, Capacity: 240, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 8301, Type: Colored glass, Capacity: 1550, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 10440, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 11613, Type: Paper, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 17490, Type: Paper, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 22164, Type: Textile, Capacity: 1000, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 11612, Type: Paper, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 23952, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 4136 4477 4478 4481 4482 6198 6199 6715 6716 6719 6720 6723 6724 7677 7678 8300 8301 10440 10614 10660 10879 10881 10883 10897 10974 11119 11612 11613 11800 11844 12090 12092 12093 12109 12189 12346 13657 14376 17490 18509 19762 22119 22164 22211 22212 22698 23200 23947 23948 23949 23950 23953 23954 24045 24046 24047 24048 24049 24051 24053 24054 24055 25733 26115 26221 26242\n"
        "ID: 6298, Type: Clear glass, Capacity: 1125, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 23201, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 12095, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 12094, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 12096, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 10882, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 6297, Type: Colored glass, Capacity: 1125, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 23951, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 22119, Type: Textile, Capacity: 1000, Address: Prazska 702, Neighbors: 4135 4136 4481 4482 6198 6199 6297 6298 6719 6720 6723 6724 8300 8301 10440 10660 10879 10882 10897 10974 11119 11612 11613 11844 12090 12094 12095 12096 12109 12189 12346 13657 14376 17490 18509 22164 22212 22698 23200 23201 23947 23948 23949 23951 23952 23953 23954 24045 24048 24053 24054 24055 26115 26242\n"
        "ID: 7678, Type: Clear glass, Capacity: 2500, Address: Prazska 702, Neighbors: 4135 14376 22698 23947 23952\n");
    strcat(correct_output, "ID: 10883, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 702, Neighbors: 4135 14376 22698 23947 23952\n"
        "ID: 7677, Type: Colored glass, Capacity: 2500, Address: Prazska 702, Neighbors: 4135 14376 22698 23947 23952\n"
        "ID: 4477, Type: Colored glass, Capacity: 1125, Address: Prazska 174, Neighbors: 6198 6199 6715 6716 8300 8301 10440 10614 10660 11612 11613 11800 11844 14376 17490 19762 22164 22211 22698 23947 23948 23949 23952 24045 24046 24047 24048 24049 25733 26115 26221\n"
        "ID: 12093, Type: Paper, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 4478, Type: Clear glass, Capacity: 1125, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 12092, Type: Paper, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 10881, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 23950, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 24051, Type: Biodegradable waste, Capacity: 240, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 4135, Type: Colored glass, Capacity: 1125, Address: Vzhledna 108, Neighbors: 4481 4482 6297 6298 6719 6720 6723 6724 7677 7678 8300 8301 10440 10879 10882 10883 10897 10974 11612 11613 12090 12094 12095 12096 12109 12189 13657 14376 17490 18509 22119 22164 22212 23201 23947 23948 23949 23951 23952 23953 24045 24053 24054 26242\n"
        "ID: 4136, Type: Clear glass, Capacity: 1125, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 12346, Type: Paper, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 23954, Type: Plastics and Aluminium, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 23200, Type: Paper, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 24055, Type: Biodegradable waste, Capacity: 240, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 11119, Type: Plastics and Aluminium, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 6716, Type: Clear glass, Capacity: 1125, Address: K Berce 721, Neighbors: 4477 4478 10881 12092 12093 22698 23947 23950 23952 24049 24051\n"
        "ID: 6715, Type: Colored glass, Capacity: 1125, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 24047, Type: Biodegradable waste, Capacity: 240, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 10614, Type: Plastics and Aluminium, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 22211, Type: Textile, Capacity: 1000, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 19762, Type: Plastics and Aluminium, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 26221, Type: Biodegradable waste, Capacity: 240, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 11800, Type: Paper, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 25733, Type: Paper, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 24046, Type: Biodegradable waste, Capacity: 240, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 22698, Type: Plastics and Aluminium, Capacity: 1100, Address: 466, Neighbors: 4477 4478 4481 4482 6297 6298 6715 6716 7677 7678 8300 8301 10440 10614 10881 10882 10883 10974 11612 11613 11800 12092 12093 12094 12095 12096 12189 14376 17490 19762 22119 22164 22211 23201 23947 23948 23949 23950 23951 23952 23953 24045 24046 24047 24049 24051 24053 24054 25733 26221 26242\n"
        "ID: 6198, Type: Colored glass, Capacity: 1125, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n"
        "ID: 26115, Type: Biodegradable waste, Capacity: 240, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n"
        "ID: 10660, Type: Plastics and Aluminium, Capacity: 1100, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n");
    strcat(correct_output, "ID: 24048, Type: Biodegradable waste, Capacity: 240, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n"
        "ID: 11844, Type: Paper, Capacity: 1100, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n"
        "ID: 6199, Type: Clear glass, Capacity: 1125, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n"
        "ID: 14376, Type: Paper, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 4136 4477 4478 6198 6199 6297 6298 7677 7678 10660 10881 10882 10883 11119 11844 12092 12093 12094 12095 12096 12346 22119 22698 23200 23201 23947 23950 23951 23952 23954 24048 24051 24055 26115\n"
        "ID: 23953, Type: Plastics and Aluminium, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 26242, Type: Biodegradable waste, Capacity: 240, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 10974, Type: Plastics and Aluminium, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 4481, Type: Colored glass, Capacity: 1125, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 24053, Type: Biodegradable waste, Capacity: 240, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 12189, Type: Paper, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 24054, Type: Biodegradable waste, Capacity: 240, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 4482, Type: Clear glass, Capacity: 1125, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 13657, Type: Paper, Capacity: 1100, Address: Prima 331, Neighbors: 4135 4136 6723 6724 10879 11119 12090 12346 22119 22212 23200 23947 23952 23954 24055\n"
        "ID: 10897, Type: Plastics and Aluminium, Capacity: 1100, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 6720, Type: Clear glass, Capacity: 1125, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 12109, Type: Paper, Capacity: 1100, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 6719, Type: Colored glass, Capacity: 1125, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 18509, Type: Plastics and Aluminium, Capacity: 1100, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 6724, Type: Clear glass, Capacity: 1125, Address: Pracata 783, Neighbors: 4135 6719 6720 10897 12109 13657 18509 22119 23947 23952\n"
        "ID: 6723, Type: Colored glass, Capacity: 1125, Address: Pracata 783, Neighbors: 4135 13657 22119 23947 23952\n"
        "ID: 12090, Type: Paper, Capacity: 1100, Address: Pracata 783, Neighbors: 4135 13657 22119 23947 23952\n"
        "ID: 10879, Type: Plastics and Aluminium, Capacity: 1100, Address: Pracata 783, Neighbors: 4135 13657 22119 23947 23952\n"
        "ID: 22212, Type: Textile, Capacity: 1000, Address: Pracata 783, Neighbors: 4135 13657 22119 23947 23952\n"
        "ID: 24049, Type: Biodegradable waste, Capacity: 240, Address: 313, Neighbors: 4477 6715 6716 10614 11800 19762 22211 22698 23947 23952 24046 24047 25733 26221\n");


    CHECK_FILE(stderr, "" /* STDERR is empty*/);
    ASSERT_FILE(stdout, correct_output);
}

TEST(my_test_waste_filter_type_A)
{
    int rv = 0; /* return value of main()*/
    CHECK(app_main_args("-t", "A", CONTAINERS_FILE, PATHS_FILE) == rv);

    /* TIP: Use ‹app_main()› to test the program without arguments. */

    const char *correct_output =
        "ID: 23947, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4136 4477 4478 4481 4482 6198 6199 6297 6298 6715 6716 6719 6720 6723 6724 7677 7678 10614 10660 10879 10881 10882 10883 10897 10974 11119 11800 11844 12090 12092 12093 12094 12095 12096 12109 12189 12346 13657 14376 18509 19762 22119 22211 22212 22698 23200 23201 23950 23951 23952 23953 23954 24046 24047 24048 24049 24051 24053 24054 24055 25733 26115 26221 26242\n"
        "ID: 23948, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 23949, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 10440, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 23952, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 4136 4477 4478 4481 4482 6198 6199 6715 6716 6719 6720 6723 6724 7677 7678 8300 8301 10440 10614 10660 10879 10881 10883 10897 10974 11119 11612 11613 11800 11844 12090 12092 12093 12109 12189 12346 13657 14376 17490 18509 19762 22119 22164 22211 22212 22698 23200 23947 23948 23949 23950 23953 23954 24045 24046 24047 24048 24049 24051 24053 24054 24055 25733 26115 26221 26242\n"
        "ID: 10882, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 23951, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 10883, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 702, Neighbors: 4135 14376 22698 23947 23952\n"
        "ID: 10881, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 23950, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 23954, Type: Plastics and Aluminium, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 11119, Type: Plastics and Aluminium, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 10614, Type: Plastics and Aluminium, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 19762, Type: Plastics and Aluminium, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 22698, Type: Plastics and Aluminium, Capacity: 1100, Address: 466, Neighbors: 4477 4478 4481 4482 6297 6298 6715 6716 7677 7678 8300 8301 10440 10614 10881 10882 10883 10974 11612 11613 11800 12092 12093 12094 12095 12096 12189 14376 17490 19762 22119 22164 22211 23201 23947 23948 23949 23950 23951 23952 23953 24045 24046 24047 24049 24051 24053 24054 25733 26221 26242\n"
        "ID: 10660, Type: Plastics and Aluminium, Capacity: 1100, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n"
        "ID: 23953, Type: Plastics and Aluminium, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 10974, Type: Plastics and Aluminium, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 10897, Type: Plastics and Aluminium, Capacity: 1100, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 18509, Type: Plastics and Aluminium, Capacity: 1100, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 10879, Type: Plastics and Aluminium, Capacity: 1100, Address: Pracata 783, Neighbors: 4135 13657 22119 23947 23952\n"
    ;

    ASSERT_FILE(stdout, correct_output);
    CHECK_FILE(stderr, "" /* STDERR is empty*/);
}

TEST(my_test_capacity_filter)
{
    int rv = 0; /* return value of main()*/
    CHECK(app_main_args("-c", "500-1100", CONTAINERS_FILE, PATHS_FILE) == rv);

    /* TIP: Use ‹app_main()› to test the program without arguments. */

    char correct_output[6666];
    strcat(correct_output, "ID: 23947, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4136 4477 4478 4481 4482 6198 6199 6297 6298 6715 6716 6719 6720 6723 6724 7677 7678 10614 10660 10879 10881 10882 10883 10897 10974 11119 11800 11844 12090 12092 12093 12094 12095 12096 12109 12189 12346 13657 14376 18509 19762 22119 22211 22212 22698 23200 23201 23950 23951 23952 23953 23954 24046 24047 24048 24049 24051 24053 24054 24055 25733 26115 26221 26242\n"
        "ID: 23948, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 23949, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 10440, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 11613, Type: Paper, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 17490, Type: Paper, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 22164, Type: Textile, Capacity: 1000, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 11612, Type: Paper, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 23952, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 4136 4477 4478 4481 4482 6198 6199 6715 6716 6719 6720 6723 6724 7677 7678 8300 8301 10440 10614 10660 10879 10881 10883 10897 10974 11119 11612 11613 11800 11844 12090 12092 12093 12109 12189 12346 13657 14376 17490 18509 19762 22119 22164 22211 22212 22698 23200 23947 23948 23949 23950 23953 23954 24045 24046 24047 24048 24049 24051 24053 24054 24055 25733 26115 26221 26242\n"
        "ID: 23201, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 12095, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 12094, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 12096, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 10882, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 23951, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 22119, Type: Textile, Capacity: 1000, Address: Prazska 702, Neighbors: 4135 4136 4481 4482 6198 6199 6297 6298 6719 6720 6723 6724 8300 8301 10440 10660 10879 10882 10897 10974 11119 11612 11613 11844 12090 12094 12095 12096 12109 12189 12346 13657 14376 17490 18509 22164 22212 22698 23200 23201 23947 23948 23949 23951 23952 23953 23954 24045 24048 24053 24054 24055 26115 26242\n"
        "ID: 10883, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 702, Neighbors: 4135 14376 22698 23947 23952\n"
        "ID: 12093, Type: Paper, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 12092, Type: Paper, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 10881, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 23950, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 12346, Type: Paper, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 23954, Type: Plastics and Aluminium, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 23200, Type: Paper, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n");
    strcat(correct_output, "ID: 11119, Type: Plastics and Aluminium, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 10614, Type: Plastics and Aluminium, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 22211, Type: Textile, Capacity: 1000, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 19762, Type: Plastics and Aluminium, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 11800, Type: Paper, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 25733, Type: Paper, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 22698, Type: Plastics and Aluminium, Capacity: 1100, Address: 466, Neighbors: 4477 4478 4481 4482 6297 6298 6715 6716 7677 7678 8300 8301 10440 10614 10881 10882 10883 10974 11612 11613 11800 12092 12093 12094 12095 12096 12189 14376 17490 19762 22119 22164 22211 23201 23947 23948 23949 23950 23951 23952 23953 24045 24046 24047 24049 24051 24053 24054 25733 26221 26242\n"
        "ID: 10660, Type: Plastics and Aluminium, Capacity: 1100, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n"
        "ID: 11844, Type: Paper, Capacity: 1100, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n"
        "ID: 14376, Type: Paper, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 4136 4477 4478 6198 6199 6297 6298 7677 7678 10660 10881 10882 10883 11119 11844 12092 12093 12094 12095 12096 12346 22119 22698 23200 23201 23947 23950 23951 23952 23954 24048 24051 24055 26115\n"
        "ID: 23953, Type: Plastics and Aluminium, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 10974, Type: Plastics and Aluminium, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 12189, Type: Paper, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 13657, Type: Paper, Capacity: 1100, Address: Prima 331, Neighbors: 4135 4136 6723 6724 10879 11119 12090 12346 22119 22212 23200 23947 23952 23954 24055\n"
        "ID: 10897, Type: Plastics and Aluminium, Capacity: 1100, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 12109, Type: Paper, Capacity: 1100, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 18509, Type: Plastics and Aluminium, Capacity: 1100, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 12090, Type: Paper, Capacity: 1100, Address: Pracata 783, Neighbors: 4135 13657 22119 23947 23952\n"
        "ID: 10879, Type: Plastics and Aluminium, Capacity: 1100, Address: Pracata 783, Neighbors: 4135 13657 22119 23947 23952\n"
        "ID: 22212, Type: Textile, Capacity: 1000, Address: Pracata 783, Neighbors: 4135 13657 22119 23947 23952\n");

    ASSERT_FILE(stdout, correct_output);
    CHECK_FILE(stderr, "" /* STDERR is empty*/);
}

TEST(my_test_all_filters_public)
{
    int rv = 0; /* return value of main()*/
    CHECK(app_main_args("-c", "500-1100", "-t", "PA", "-p", "Y", CONTAINERS_FILE, PATHS_FILE) == rv);

    /* TIP: Use ‹app_main()› to test the program without arguments. */

    char correct_output[5678];
    strcat(correct_output, "ID: 23947, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4136 4477 4478 4481 4482 6198 6199 6297 6298 6715 6716 6719 6720 6723 6724 7677 7678 10614 10660 10879 10881 10882 10883 10897 10974 11119 11800 11844 12090 12092 12093 12094 12095 12096 12109 12189 12346 13657 14376 18509 19762 22119 22211 22212 22698 23200 23201 23950 23951 23952 23953 23954 24046 24047 24048 24049 24051 24053 24054 24055 25733 26115 26221 26242\n"
        "ID: 23948, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 23949, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 10440, Type: Plastics and Aluminium, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 11613, Type: Paper, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 17490, Type: Paper, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 11612, Type: Paper, Capacity: 1100, Address: Bosonozske namesti 669, Neighbors: 4135 4477 22119 22698 23952\n"
        "ID: 23952, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 4136 4477 4478 4481 4482 6198 6199 6715 6716 6719 6720 6723 6724 7677 7678 8300 8301 10440 10614 10660 10879 10881 10883 10897 10974 11119 11612 11613 11800 11844 12090 12092 12093 12109 12189 12346 13657 14376 17490 18509 19762 22119 22164 22211 22212 22698 23200 23947 23948 23949 23950 23953 23954 24045 24046 24047 24048 24049 24051 24053 24054 24055 25733 26115 26221 26242\n"
        "ID: 23201, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 12095, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 12094, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 12096, Type: Paper, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 10882, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 23951, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 79, Neighbors: 4135 14376 22119 22698 23947\n"
        "ID: 10883, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 702, Neighbors: 4135 14376 22698 23947 23952\n"
        "ID: 12093, Type: Paper, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 12092, Type: Paper, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 10881, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 23950, Type: Plastics and Aluminium, Capacity: 1100, Address: Prazska 174, Neighbors: 6716 14376 22698 23947 23952\n"
        "ID: 12346, Type: Paper, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n");
    strcat(correct_output, "ID: 23954, Type: Plastics and Aluminium, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 23200, Type: Paper, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 11119, Type: Plastics and Aluminium, Capacity: 1100, Address: Vzhledna 108, Neighbors: 13657 14376 22119 23947 23952\n"
        "ID: 10614, Type: Plastics and Aluminium, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 19762, Type: Plastics and Aluminium, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 11800, Type: Paper, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 25733, Type: Paper, Capacity: 1100, Address: K Berce 721, Neighbors: 4477 22698 23947 23952 24049\n"
        "ID: 22698, Type: Plastics and Aluminium, Capacity: 1100, Address: 466, Neighbors: 4477 4478 4481 4482 6297 6298 6715 6716 7677 7678 8300 8301 10440 10614 10881 10882 10883 10974 11612 11613 11800 12092 12093 12094 12095 12096 12189 14376 17490 19762 22119 22164 22211 23201 23947 23948 23949 23950 23951 23952 23953 24045 24046 24047 24049 24051 24053 24054 25733 26221 26242\n"
        "ID: 10660, Type: Plastics and Aluminium, Capacity: 1100, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n"
        "ID: 11844, Type: Paper, Capacity: 1100, Address: 466, Neighbors: 4477 14376 22119 23947 23952\n"
        "ID: 14376, Type: Paper, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 4136 4477 4478 6198 6199 6297 6298 7677 7678 10660 10881 10882 10883 11119 11844 12092 12093 12094 12095 12096 12346 22119 22698 23200 23201 23947 23950 23951 23952 23954 24048 24051 24055 26115\n"
        "ID: 23953, Type: Plastics and Aluminium, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 10974, Type: Plastics and Aluminium, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 12189, Type: Paper, Capacity: 1100, Address: Sevcenkova 572, Neighbors: 4135 22119 22698 23947 23952\n"
        "ID: 13657, Type: Paper, Capacity: 1100, Address: Prima 331, Neighbors: 4135 4136 6723 6724 10879 11119 12090 12346 22119 22212 23200 23947 23952 23954 24055\n"
        "ID: 10897, Type: Plastics and Aluminium, Capacity: 1100, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 12109, Type: Paper, Capacity: 1100, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 18509, Type: Plastics and Aluminium, Capacity: 1100, Address: Prima 331, Neighbors: 4135 6724 22119 23947 23952\n"
        "ID: 12090, Type: Paper, Capacity: 1100, Address: Pracata 783, Neighbors: 4135 13657 22119 23947 23952\n"
        "ID: 10879, Type: Plastics and Aluminium, Capacity: 1100, Address: Pracata 783, Neighbors: 4135 13657 22119 23947 23952\n");

    ASSERT_FILE(stdout, correct_output);
    CHECK_FILE(stderr, "" /* STDERR is empty*/);
}

TEST(my_test_all_filters_not_public)
{
    int rv = 0; /* return value of main()*/
    CHECK(app_main_args("-c", "500-1100", "-t", "PA", "-p", "N", CONTAINERS_FILE, PATHS_FILE) == rv);

    /* TIP: Use ‹app_main()› to test the program without arguments. */

    const char *correct_output = "";

    ASSERT_FILE(stdout, correct_output);
    CHECK_FILE(stderr, "" /* STDERR is empty*/);
}

TEST(my_test_wrong_option_error)
{
    CHECK(app_main_args("-f", CONTAINERS_FILE, PATHS_FILE) != 0);

    CHECK_NOT_EMPTY(stderr);
}

TEST(my_test_similar_filter_error)
{
    CHECK(app_main_args("-t", "PA", "-t", "PA", CONTAINERS_FILE, PATHS_FILE) != 0);

    CHECK_NOT_EMPTY(stderr);
}

TEST(my_test_sites)
{
    int rv = 0; /* return value of main()*/
    CHECK(app_main_args("-s", CONTAINERS_FILE, PATHS_FILE) == rv);

    /* TIP: Use ‹app_main()› to test the program without arguments. */

    const char *correct_output = 
        "1;APBGCT;2,3,4,5,6,7,8,9,10,11\n"
        "2;APGC;1,3,4,5,6,7,8,9,10,11\n"
        "3;AGCT;1,2,5,7,8,9,10\n"
        "4;APBGC;1,2,6,7,8,11\n"
        "5;APBGC;1,2,3,8,9,10\n"
        "6;APBGCT;1,2,4,7,11\n"
        "7;APBGC;1,2,3,4,6,8,11\n"
        "8;APBGC;1,2,3,4,5,7\n"
        "9;APGC;1,2,3,5,10\n"
        "10;APGCT;1,2,3,5,9\n"
        "11;B;1,2,4,6,7\n"
    ;

    ASSERT_FILE(stdout, correct_output);
    CHECK_FILE(stderr, "" /* STDERR is empty*/);
}