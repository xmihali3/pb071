#ifndef PRINT_DATA_H
#define PRINT_DATA_H

struct container
{
    size_t id;
    size_t capacity;
    const char *waste_type;
    const char *street;
    const char *number;
    size_t *neighbors;
    size_t neighbors_count;
    const char *public;
};
int add_neighbor(struct container *container, size_t new_neighbor);
int check_neighbors(struct container *container);
int compare_neighbors(const void *neighbor_a, const void *neighbor_b);
int print_data_line(struct container *container);
int load_data();

#endif