#include "data_source.h"
#include "filter_data.h"
#include "print_data.h"
#include "sites.h"

#include <ctype.h>
#include <setjmp.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool valid_container_data(size_t i)
{   
    WasteType waste_type = waste_type_from_string(get_container_waste_type(i));
    return (get_container_id(i) != NULL
            && get_container_capacity(i) != NULL
            && strtoul(get_container_id(i), NULL, 10) != 0
            && strtoul(get_container_capacity(i), NULL, 10) != 0
            && get_container_x(i) != NULL
            && get_container_y(i) != NULL
            && strtof(get_container_x(i), NULL) != 0.0
            && strtof(get_container_y(i), NULL) != 0.0
            && waste_type_acronym(waste_type) != 'X'
            && (strcmp(get_container_public(i), "Y")
                || strcmp(get_container_public(i), "N"))
            && (get_container_number(i) == NULL
                || strtoul(get_container_number(i), NULL, 10) != 0));
}

bool valid_container_ids(size_t i, size_t size, size_t *container_ids)
{
    size_t path_a = strtoul(get_path_a_id(i), NULL, 10);
    size_t path_b = strtoul(get_path_b_id(i), NULL, 10);
    if (path_a == path_b) {
        return false;
    }

    bool a_valid, b_valid = false;
    for (size_t j = 0; j < size; j++) {
        if (container_ids[j] == path_a) {
            a_valid = true;
        }
        if (container_ids[j] == path_b) {
            b_valid = true;
        }
    }
    return a_valid && b_valid;
}

bool valid_paths_data(size_t size, size_t *container_ids)
{
    size_t i = -1;
    while (get_path_a_id(++i) != NULL) {
        if (get_path_a_id(i) == NULL
            || get_path_b_id(i) == NULL
            || get_path_distance(i) == NULL
            || strtoul(get_path_a_id(i), NULL, 10) == 0
            || strtoul(get_path_b_id(i), NULL, 10) == 0
            || strtoul(get_path_distance(i), NULL, 10) == 0
            || !valid_container_ids(i, size, container_ids)) {
            return false;
        }
    }
    return get_path_b_id(i) == NULL && get_path_distance(i) == NULL;
}

bool valid_data()
{
    size_t *container_ids = 0;
    size_t size = -1;
    while (get_container_id(++size) != NULL) {
        if (!valid_container_data(size)) {
            free(container_ids);
            return false;
        }
        container_ids = realloc(container_ids, (size + 1) * sizeof(size_t));
        if (container_ids == NULL) {
            free(container_ids);
            return false;
        }
        container_ids[size] = strtoul(get_container_id(size), NULL, 10);
    }

    if (!valid_paths_data(size, container_ids)) {
        free(container_ids);
        return false;
    }
    free(container_ids);
    return true;
}

bool valid_args(int argc, char *argv[])
{
    if (argc == 3 || (argc == 4 && !strcmp(argv[1], "-s"))) {
        return true;
    }

    if (argc % 2 == 0) {
        perror("Wrong arguments count\n");
        return false;
    }

    bool filt_type = false, filt_cap = false, filt_pub = false;
    for (int i = 1; i < argc - 2; i += 2) {
        if (!strcmp(argv[i], "-t")) {
            if (filt_type) {
                perror("Repeating filter type\n");
                return false;
            }
            filt_type = true;
        } else if (!strcmp(argv[i], "-c")) {
            if (filt_cap) {
                perror("Repeating filter type\n");
                return false;
            }
            filt_cap = true;
        } else if (!strcmp(argv[i], "-p")) {
            if (filt_pub) {
                perror("Repeating filter type\n");
                return false;
            }
            filt_pub = true;
        } else {
            perror("Unknown argument\n");
            return false;
        }
    }
    return true;
}

int main(int argc, char *argv[])
{
    if (argc < 3) {
        perror("Not enough files in arguments\n");
        return EXIT_FAILURE;
    }

    if (!valid_args(argc, argv)) {
        return EXIT_FAILURE;
    }

    if (!init_data_source(argv[argc - 2], argv[argc - 1])) {
        perror("Bad data or incorrect data paths\n");
        return EXIT_FAILURE;
    }

    if (!valid_data()) {
        perror("Bad data\n");
        return EXIT_FAILURE;
    }

    if (argc == 3) {
        return load_data();
    }

    if (argc == 4) {
        return list_of_sites();
    }

    return filter_data(argc, argv);
}
