#include "data_source.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct container
{
    size_t id;
    size_t capacity;
    const char *waste_type;
    const char *street;
    const char *number;
    size_t *neighbors;
    size_t neighbors_count;
    const char *public;
};

int add_neighbor(struct container *container, size_t new_neighbor)
{
    for (size_t i = 0; i < container->neighbors_count; i++) {
        if (container->neighbors[i] == new_neighbor) {
            return EXIT_SUCCESS;
        }
    }
    container->neighbors = realloc(container->neighbors, (container->neighbors_count + 1) * sizeof(size_t));
    if (container->neighbors == NULL) {
        perror("memory allocation fail\n");
        return EXIT_FAILURE;
    }
    container->neighbors[container->neighbors_count++] = new_neighbor;
    return EXIT_SUCCESS;
}

int check_neighbors(struct container *container)
{
    size_t path = -1;
    while (get_path_a_id(++path) != NULL) {
        size_t a_id = strtoul(get_path_a_id(path), NULL, 10);
        size_t b_id = strtoul(get_path_b_id(path), NULL, 10);
        if (b_id == container->id) {
            if (add_neighbor(container, a_id)) {
                return EXIT_FAILURE;
            }
        } else if (a_id == container->id) {
            if (add_neighbor(container, b_id)) {
                return EXIT_FAILURE;
            }
        }
    }
    return EXIT_SUCCESS;
}

int compare_neighbors(const void *neighbor_a, const void *neighbor_b)
{
    size_t num_a = *(size_t *) neighbor_a;
    size_t num_b = *(size_t *) neighbor_b;
    if (num_a < num_b) {
        return -1;
    }
    return 1;
}

int print_data_line(struct container *container)
{
    printf("ID: %ld, Type: %s, Capacity: %ld, Address:", container->id, container->waste_type, container->capacity);
    if (strcmp(container->street, "")) {
        printf(" %s", container->street);
    }

    if (strcmp(container->number, "")) {
        printf(" %s", container->number);
    }

    printf(", Neighbors:");
    if (check_neighbors(container)) {
        free(container->neighbors);
        return EXIT_FAILURE;
    }
    qsort(container->neighbors, container->neighbors_count, sizeof(size_t), compare_neighbors);
    for (size_t i = 0; i < container->neighbors_count; i++) {
        printf(" %ld", container->neighbors[i]);
    }
    free(container->neighbors);
    printf("\n");
    return EXIT_SUCCESS;
}

int load_data()
{
    size_t i = -1;
    struct container container;
    while (get_container_id(++i) != NULL) {
        container.id = strtoul(get_container_id(i), NULL, 10);
        container.capacity = strtoul(get_container_capacity(i), NULL, 10);
        container.number = get_container_number(i);
        container.street = get_container_street(i);
        container.waste_type = get_container_waste_type(i);
        container.neighbors = NULL;
        container.neighbors_count = 0;
        if (print_data_line(&container)) {
            destroy_data_source();
            return EXIT_FAILURE;
        }
    }
    destroy_data_source();
    return EXIT_SUCCESS;
}
